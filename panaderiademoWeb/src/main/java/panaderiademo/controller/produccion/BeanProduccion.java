package panaderiademo.controller.produccion;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import panaderiademo.controller.cliente.JSFUtil;
import panaderiademo.model.core.entities.ProMateriaPrima;
import panaderiademo.model.core.entities.ProMedida;
import panaderiademo.model.produccion.managers.ManagerProduccion;

@Named
@SessionScoped
public class BeanProduccion implements Serializable {
	@EJB
	private ManagerProduccion managerProduccion;
	private List<ProMateriaPrima> materiaPrima;	
	private List<ProMedida> medida;
	private int medidaseleccionada;
	private ProMateriaPrima nuevaMateriaPrima;
	private ProMateriaPrima edicionMateriaPrima;
	private ProMedida nuevaMedida;
	
	private static final long serialVersionUID = 1L;

	public BeanProduccion() {
	
	}
	@PostConstruct
	public void inicializar() {
		materiaPrima = managerProduccion.findAllProMateriaPrima();
		medida = managerProduccion.findAllProMedida();
	}
	
	public String actionMenuInventario() {
		materiaPrima = managerProduccion.findAllProMateriaPrima();
		return "inventario";
	}
	public String actionMenuNuevoProducto() {
		nuevaMateriaPrima = new ProMateriaPrima();
		return "productos_nuevos";
	}
	public String actionMateriaPrima() {
		nuevaMedida = new ProMedida();
		return "medida";
	}
	public void actionListenerSeleccionarProductoEditar(ProMateriaPrima ProMateriaPrima) {
		edicionMateriaPrima = ProMateriaPrima;		
		medidaseleccionada = ProMateriaPrima.getProMedida().getIdProMedida();
	}
	
	public void actionListenerInsertarNuevoProducto() {				
		try {
			ProMedida medida = managerProduccion.findProMedidaById(medidaseleccionada);
			nuevaMateriaPrima.setProMedida(medida);
			managerProduccion.insertarProducto(nuevaMateriaPrima);
			materiaPrima = managerProduccion.findAllProMateriaPrima();
			JSFUtil.crearMensajeInfo("Materia Prima Registrada.");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}
	public void actionListenerInsertarMedida() {
		try {
			managerProduccion.insertarMedida(nuevaMedida);
			medida = managerProduccion.findAllProMedida();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void actionListenerEliminarMedida(ProMedida medidaE) {
		try {
		managerProduccion.eliminarMedida(medidaE);
		medida = managerProduccion.findAllProMedida();
		} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		}
		}
	
	public void actionListenerActualizarProducto() {		
		try {
			ProMedida medida = managerProduccion.findProMedidaById(medidaseleccionada);
			edicionMateriaPrima.setProMedida(medida);
			managerProduccion.actualizarProducto(edicionMateriaPrima);
			materiaPrima = managerProduccion.findAllProMateriaPrima();
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
	
	public void actionListenerEliminarProducto(ProMateriaPrima materiaPrimaE) {
		try {
			managerProduccion.eliminarProducto(materiaPrimaE);
			materiaPrima = managerProduccion.findAllProMateriaPrima();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}
	public void actionListenerCambiarEstado(ProMateriaPrima materiaPrimaEstado) {
		try {
			managerProduccion.cambiarEstado(materiaPrimaEstado);
			materiaPrima = managerProduccion.findAllProMateriaPrima();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public String actionReporte() {
		Map<String, Object> parametros = new HashMap<String, Object>();
		/*
		 * parametros.put("p_titulo_principal",p_titulo_principal);
		 * parametros.put("p_titulo",p_titulo);
		 */
		FacesContext context = FacesContext.getCurrentInstance();
		ServletContext servletContext = (ServletContext) context.getExternalContext().getContext();
		String ruta = servletContext.getRealPath("produccion/inventario.jasper");
		System.out.println(ruta);
		HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
		response.addHeader("Content-disposition", "attachment;filename=reporte.pdf");
		response.setContentType("application/pdf");
		try {
			Class.forName("org.postgresql.Driver");
			Connection connection = null;
			connection = DriverManager.getConnection("jdbc:postgresql://192.100.198.141:5432/bapulamarinc",
					"bapulamarinc", "1727468512");
			JasperPrint impresion = JasperFillManager.fillReport(ruta, parametros, connection);
			JasperExportManager.exportReportToPdfStream(impresion, response.getOutputStream());
			context.getApplication().getStateManager().saveView(context);
			System.out.println("reporte generado.");
			context.responseComplete();
		} catch (Exception e) {
			JSFUtil.crearMensajeEror(e.getMessage());
			e.printStackTrace();
		}
		return "";
	}
	
	public List<ProMateriaPrima> getMateriaPrima() {
		return materiaPrima;
	}
	public void setMateriaPrima(List<ProMateriaPrima> materiaPrima) {
		this.materiaPrima = materiaPrima;
	}
	public ProMateriaPrima getNuevaMateriaPrima() {
		return nuevaMateriaPrima;
	}
	public void setNuevaMateriaPrima(ProMateriaPrima nuevaMateriaPrima) {
		this.nuevaMateriaPrima = nuevaMateriaPrima;
	}
	public ProMateriaPrima getEdicionMateriaPrima() {
		return edicionMateriaPrima;
	}
	public void setEdicionMateriaPrima(ProMateriaPrima edicionMateriaPrima) {
		this.edicionMateriaPrima = edicionMateriaPrima;
	}
	public List<ProMedida> getMedida() {
		return medida;
	}
	public void setMedida(List<ProMedida> medida) {
		this.medida = medida;
	}
	public int getMedidaseleccionada() {
		return medidaseleccionada;
	}
	public void setMedidaseleccionada(int medidaseleccionada) {
		this.medidaseleccionada = medidaseleccionada;
	}
	public ProMedida getNuevaMedida() {
		return nuevaMedida;
	}
	public void setNuevaMedida(ProMedida nuevaMedida) {
		this.nuevaMedida = nuevaMedida;
	}

}
