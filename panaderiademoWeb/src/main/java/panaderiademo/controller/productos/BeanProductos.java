package panaderiademo.controller.productos;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;

import panaderiademo.controller.JSFUtil;
import panaderiademo.model.core.entities.InvCategoria;
import panaderiademo.model.core.entities.InvProducto;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import panaderiademo.model.productos.managers.ManagerProducto;

@Named
@SessionScoped
public class BeanProductos implements Serializable {

	private static final long serialVersionUID = 1L;
	@EJB
	private ManagerProducto managerProductos;
	private List<InvProducto> listaProductos;
	private InvProducto producto;
	private InvCategoria categoria;
	private int categoriaSeleccionada;
	private List<InvCategoria> listaCategorias;


	private InvProducto productoSeleccionado;
	private InvCategoria CategoriaSele;
	private int idCategoriaEdicion;

	public BeanProductos() {
	}

	@PostConstruct

	public void inicializar() {
		listaProductos = managerProductos.findAllProductos();
		listaCategorias = managerProductos.findAllCategorias();
		producto = new InvProducto();
		categoria = new InvCategoria();
	}

	public String actionReporte() {
		Map<String, Object> parametros = new HashMap<String, Object>();
		/*
		 * parametros.put("p_titulo_principal",p_titulo_principal);
		 * parametros.put("p_titulo",p_titulo);
		 */
		FacesContext context = FacesContext.getCurrentInstance();
		ServletContext servletContext = (ServletContext) context.getExternalContext().getContext();
		String ruta = servletContext.getRealPath("inventario/reporteProyecto.jasper");
		System.out.println(ruta);
		HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
		response.addHeader("Content-disposition", "attachment;filename=reporte.pdf");
		response.setContentType("application/pdf");
		try {
			Class.forName("org.postgresql.Driver");
			Connection connection = null;
			connection = DriverManager.getConnection("jdbc:postgresql://192.100.198.141:5432/bapulamarinc",
					"bapulamarinc", "1727468512");
			JasperPrint impresion = JasperFillManager.fillReport(ruta, parametros, connection);
			JasperExportManager.exportReportToPdfStream(impresion, response.getOutputStream());
			context.getApplication().getStateManager().saveView(context);
			System.out.println("reporte generado.");
			context.responseComplete();
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
		return "";
	}

	public void actionListenerCambiarEstadoDisponible(InvProducto producto) {
		managerProductos.cambiarEstadoDisponible(producto);
		JSFUtil.crearMensajeINFO("Estado cambiado");
	}

	public void actionListenerInsertarProducto() {
		try {

			managerProductos.insertarProducto(producto, categoriaSeleccionada);
			listaProductos = managerProductos.findAllProductos();
			producto = new InvProducto();
			JSFUtil.crearMensajeINFO("Producto insertado");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}

	}

	public void actionListenerInsertarCategoria() {
		try {

			managerProductos.insertarCategoria(categoria);
			listaCategorias = managerProductos.findAllCategorias();
			categoria = new InvCategoria();
			JSFUtil.crearMensajeINFO("Categoria insertada");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}

	}

	public void actionListenerEliminarProducto(int idProducto) {
		try {
			managerProductos.eliminarProducto(idProducto);
			listaProductos = managerProductos.findAllProductos();
			JSFUtil.crearMensajeINFO("Producto Eliminado correctamente");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}

	public void actionListenerEliminarCategoria(int idCategoria) {
		try {
			managerProductos.eliminarCategoria(idCategoria);
			;
			listaCategorias = managerProductos.findAllCategorias();
			JSFUtil.crearMensajeINFO("Categoria Eliminada correctamente");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}

	public void actionListenerActualizarProducto() {
		managerProductos.actualizarProducto(productoSeleccionado, idCategoriaEdicion);
		listaProductos = managerProductos.findAllProductos();
		JSFUtil.crearMensajeINFO("Producto actualizado.");
	}

	public void actionListenerActualizarCategoria() {
		managerProductos.actualizarCategoria(CategoriaSele);
		listaCategorias = managerProductos.findAllCategorias();
		JSFUtil.crearMensajeINFO("Categoria actualizada");
	}

	public void actionListenerSeleccionarProductoEdicion(InvProducto producto) {
		productoSeleccionado = producto;
		idCategoriaEdicion = producto.getInvCategoria().getIdInvCategoria();
	}

	public void actionListenerSeleccionarCategoriaEdicion(InvCategoria categoria) {
		CategoriaSele = categoria;
	}

	public List<InvProducto> getListaProductos() {
		return listaProductos;
	}

	public void setListaProductos(List<InvProducto> listaProductos) {
		this.listaProductos = listaProductos;
	}

	public InvProducto getProducto() {
		return producto;
	}

	public void setProducto(InvProducto producto) {
		this.producto = producto;
	}

	public InvCategoria getCategoria() {
		return categoria;
	}

	public void setCategoria(InvCategoria categoria) {
		this.categoria = categoria;
	}

	public int getCategoriaSeleccionada() {
		return categoriaSeleccionada;
	}

	public void setCategoriaSeleccionada(int categoriaSeleccionada) {
		this.categoriaSeleccionada = categoriaSeleccionada;
	}

	public List<InvCategoria> getListaCategorias() {
		return listaCategorias;
	}

	public void setListaCategorias(List<InvCategoria> listaCategorias) {
		this.listaCategorias = listaCategorias;
	}

	public InvProducto getProductoSeleccionado() {
		return productoSeleccionado;
	}

	public void setProductoSeleccionado(InvProducto productoSeleccionado) {
		this.productoSeleccionado = productoSeleccionado;
	}

	public int getIdCategoriaEdicion() {
		return idCategoriaEdicion;
	}

	public void setIdCategoriaEdicion(int idCategoriaEdicion) {
		this.idCategoriaEdicion = idCategoriaEdicion;
	}

	public InvCategoria getCategoriaSele() {
		return CategoriaSele;
	}

	public void setCategoriaSele(InvCategoria categoriaSele) {
		CategoriaSele = categoriaSele;
	}


}
