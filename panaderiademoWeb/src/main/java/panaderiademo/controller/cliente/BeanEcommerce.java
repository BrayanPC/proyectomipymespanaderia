package panaderiademo.controller.cliente;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import panaderiademo.controller.JSFUtil;
import panaderiademo.model.cliente.managers.ManagerEcommerce;
import panaderiademo.model.core.entities.InvCategoria;
import panaderiademo.model.core.entities.InvProducto;
import panaderiademo.model.core.entities.VenImpuesto;
import panaderiademo.model.productos.dtos.ProductoDTO;
import panaderiademo.model.ventas.dtos.DetalleVentaDTO;
import panaderiademo.model.ventas.dtos.VentaDTO;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.text.SimpleDateFormat;

@Named
@SessionScoped
public class BeanEcommerce implements Serializable {
	private static final long serialVersionUID = 1L;

	@EJB
	private ManagerEcommerce managerEcommerce;
	private List<InvCategoria> listaCategorias;
	private List<ProductoDTO> listaProductos;
	private List<DetalleVentaDTO> listaDetalleVenta;
	private int idCatSeleccionada;
	private VentaDTO venta;
	private VenImpuesto impuesto;
	private String nombreP;

	@PostConstruct
	public void inicializar() {

	}

	public String actionMenuCatalogo() {
		nombreP = "";
		venta = new VentaDTO();
		listaProductos = managerEcommerce.findProductosByCatId(0, nombreP);
		listaCategorias = managerEcommerce.findAllCategorias();
		listaDetalleVenta = new ArrayList<DetalleVentaDTO>();
		impuesto = new VenImpuesto();
		try {
			impuesto = managerEcommerce.findImpuesto();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "catalogo";
	}

	public void acionListenerBuscarProductos() {
		listaProductos = managerEcommerce.findProductosByCatId(idCatSeleccionada, nombreP);
		if (listaProductos.size() < 1) {
			JSFUtil.crearMensajeERROR("0 Productos Econtrados");
		} else {
			JSFUtil.crearMensajeINFO(listaProductos.size() + " Productos Econtrados");
		}
	}

	public void actionListenerAgregarDetalleVenta(ProductoDTO proDto) {
		DetalleVentaDTO detalleVenta = new DetalleVentaDTO();
		detalleVenta.setIdPro(proDto.getIdPro());
		InvProducto pro = new InvProducto();
		try {
			pro = managerEcommerce.findProductoById(proDto.getIdPro());
			int id = 1;
			if (listaDetalleVenta.size() != 0) {
				id = listaDetalleVenta.get(listaDetalleVenta.size() - 1).getIdDetVen() + 1;
			}
			detalleVenta.setIdDetVen(id);
			detalleVenta.setNombrePro(pro.getNombrePro());
			detalleVenta.setCantidad(proDto.getCantidad());
			double precio = pro.getPrecioPro().doubleValue();
			detalleVenta.setPrecio(precio);
			double total = proDto.getCantidad() * precio;
			detalleVenta.setTotal(total);
			listaDetalleVenta.add(detalleVenta);
			venta = managerEcommerce.calcularTotal(listaDetalleVenta, impuesto.getValorImp().doubleValue());
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
		}
	}

	public void actionListenerEliminarProductoDetalleVenta(int idDetVen) {
		listaDetalleVenta = managerEcommerce.eliminarProductoDetalleVenta(listaDetalleVenta, idDetVen);
		venta = managerEcommerce.calcularTotal(listaDetalleVenta, impuesto.getValorImp().doubleValue());
	}

	public void actionListenerFinalizar() {
		Date fecha = new Date();
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/YYYY");
		String fechaActual = format.format(fecha);
		if (listaDetalleVenta.size() > 0) {
			try {
				venta.setIdCLi(1);
				venta.setIdUsu(1);
				venta.setFecha(fechaActual);
				venta.setIva(impuesto.getPorcentajeImp());
				venta = managerEcommerce.registrarVenta(venta, listaDetalleVenta);
				System.out.println("id: " + venta.getIdVen());
				this.actionReporte();
				this.actionMenuCatalogo();
				JSFUtil.crearMensajeINFO("Reporte Genado Exitosamente.");
			} catch (Exception e) {
				JSFUtil.crearMensajeERROR(e.getMessage());
				e.printStackTrace();
			}
		} else {
			JSFUtil.crearMensajeERROR("Lista de Venta Vacia.");
		}
	}

	public String actionReporte() {
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("idVen", venta.getIdVen());
		FacesContext context = FacesContext.getCurrentInstance();
		ServletContext servletContext = (ServletContext) context.getExternalContext().getContext();
		String ruta = servletContext.getRealPath("pedido.jasper");
		System.out.println(ruta);
		HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
		response.addHeader("Content-disposition", "attachment;filename=reporte.pdf");
		response.setContentType("application/pdf");
		try {
			Class.forName("org.postgresql.Driver");
			Connection connection = null;
			connection = DriverManager.getConnection("jdbc:postgresql://192.100.198.141:5432/bapulamarinc",
					"bapulamarinc", "1727468512");
			JasperPrint impresion = JasperFillManager.fillReport(ruta, parametros, connection);
			JasperExportManager.exportReportToPdfStream(impresion, response.getOutputStream());
			context.getApplication().getStateManager().saveView(context);
			System.out.println("reporte generado.");
			context.responseComplete();
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
		return "";
	}

	public List<InvCategoria> getListaCategorias() {
		return listaCategorias;
	}

	public void setListaCategorias(List<InvCategoria> listaCategorias) {
		this.listaCategorias = listaCategorias;
	}

	public List<ProductoDTO> getListaProductos() {
		return listaProductos;
	}

	public void setListaProductos(List<ProductoDTO> listaProductos) {
		this.listaProductos = listaProductos;
	}

	public List<DetalleVentaDTO> getListaDetalleVenta() {
		return listaDetalleVenta;
	}

	public void setListaDetalleVenta(List<DetalleVentaDTO> listaDetalleVenta) {
		this.listaDetalleVenta = listaDetalleVenta;
	}

	public int getIdCatSeleccionada() {
		return idCatSeleccionada;
	}

	public void setIdCatSeleccionada(int idCatSeleccionada) {
		this.idCatSeleccionada = idCatSeleccionada;
	}

	public VentaDTO getVenta() {
		return venta;
	}

	public void setVenta(VentaDTO venta) {
		this.venta = venta;
	}

	public VenImpuesto getImpuesto() {
		return impuesto;
	}

	public void setImpuesto(VenImpuesto impuesto) {
		this.impuesto = impuesto;
	}

	public String getNombreP() {
		return nombreP;
	}

	public void setNombreP(String nombreP) {
		this.nombreP = nombreP;
	}

}
