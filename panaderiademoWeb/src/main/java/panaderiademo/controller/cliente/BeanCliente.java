package panaderiademo.controller.cliente;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;


import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import panaderiademo.model.cliente.managers.ManagerCliente;
import panaderiademo.model.core.entities.EcoCliente;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;

@Named
@SessionScoped
public class BeanCliente implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@EJB
	private ManagerCliente managerCliente;
	private List<EcoCliente> listaClientes;
	private EcoCliente cliente;
	private EcoCliente nuevoCliente;
	private EcoCliente clienteSeleccionado;
	private boolean panelColapsado;
	private String confirmarContrasenia;

	
	@PostConstruct
	public void inicializar() {
		listaClientes = managerCliente.findAllClientes();
		System.out.println(listaClientes.size());
		cliente = new EcoCliente();
		panelColapsado = true;
	}
	
	
	public String actionReporte() {
		Map<String, Object> parametros = new HashMap<String, Object>();
		/*
		 * parametros.put("p_titulo_principal",p_titulo_principal);
		 * parametros.put("p_titulo",p_titulo);
		 */
		FacesContext context = FacesContext.getCurrentInstance();
		ServletContext servletContext = (ServletContext) context.getExternalContext().getContext();
		String ruta = servletContext.getRealPath("cliente/reporteClientes.jasper");
		System.out.println(ruta);
		HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
		response.addHeader("Content-disposition", "attachment;filename=reporteCliente.pdf");
		response.setContentType("application/pdf");
		try {
			Class.forName("org.postgresql.Driver");
			Connection connection = null;
			connection = DriverManager.getConnection("jdbc:postgresql://192.100.198.141:5432/bapulamarinc", "bapulamarinc",
					"1727468512");
			JasperPrint impresion = JasperFillManager.fillReport(ruta, parametros, connection);
			JasperExportManager.exportReportToPdfStream(impresion, response.getOutputStream());
			context.getApplication().getStateManager().saveView(context);
			System.out.println("reporte generado.");
			context.responseComplete();
		} catch (Exception e) {
			JSFUtil.crearMensajeEror(e.getMessage());
			e.printStackTrace();
		}
		return "";
	}
	
	
	public String actionMenuNuevoCliente() {
		nuevoCliente=new EcoCliente();
		return "nuevoCliente";
	}
	
	public void actionListenerColapsarPanel() {
		panelColapsado =! panelColapsado;
	}
	
	public void actionListenerRegistrarCliente(){
		try {
			
			if(cliente.getClaveCli().equals(confirmarContrasenia)) {
				managerCliente.registrarCliente(cliente);
				JSFUtil.crearMensajeInfo("Registro Exitoso");
			}else {
				JSFUtil.crearMensajeEror("Contrasenias No Coinciden");
			}			
		} catch (Exception e) {
			JSFUtil.crearMensajeEror(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void actionListenerInsertarCliente(){
		try {
			managerCliente.insertarCliente(cliente);
			listaClientes=managerCliente.findAllClientes();
			cliente = new EcoCliente();
			JSFUtil.crearMensajeInfo("Datos del Cliente Ingresados");
		} catch (Exception e) {
			JSFUtil.crearMensajeEror(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void actionListenerInsertarNuevoCliente() {
		try {
			managerCliente.insertarCliente(nuevoCliente);
			listaClientes=managerCliente.findAllClientes();
			nuevoCliente=new EcoCliente();			
			JSFUtil.crearMensajeInfo("Cliente insertado.");
		} catch (Exception e) {
			JSFUtil.crearMensajeEror("Cliente ya Registrado.");
		}
	}
	
	public void actionListenerSeleccionarCliente(EcoCliente cliente) {
		clienteSeleccionado = cliente;
	}
	
	public void actionListenerActualizarCliente() {
		try {
			managerCliente.actualizarCliente(clienteSeleccionado);
			JSFUtil.crearMensajeInfo("Cliente Actualizado Correctamente.");
		} catch (Exception e) {
			JSFUtil.crearMensajeEror(e.getMessage());
		}
	}
	
	public void actionListenerEliminarCliente(int idCLi) {
		managerCliente.eliminarCliente(idCLi);
		listaClientes=managerCliente.findAllClientes();
		JSFUtil.crearMensajeInfo("Cliente Eliminado");
	}

	public List<EcoCliente> getListaClientes() {
		return listaClientes;
	}

	public void setListaClientes(List<EcoCliente> listaClientes) {
		this.listaClientes = listaClientes;
	}

	public EcoCliente getCliente() {
		return cliente;
	}

	public void setCliente(EcoCliente cliente) {
		this.cliente = cliente;
	}

	public boolean isPanelColapsado() {
		return panelColapsado;
	}

	public void setPanelColapsado(boolean panelColapsado) {
		this.panelColapsado = panelColapsado;
	}

	public EcoCliente getNuevoCliente() {
		return nuevoCliente;
	}

	public void setNuevoCliente(EcoCliente nuevoCliente) {
		this.nuevoCliente = nuevoCliente;
	}

	public EcoCliente getClienteSeleccionado() {
		return clienteSeleccionado;
	}

	public void setClienteSeleccionado(EcoCliente clienteSeleccionado) {
		this.clienteSeleccionado = clienteSeleccionado;
	}

	public String getConfirmarContrasenia() {
		return confirmarContrasenia;
	}

	public void setConfirmarContrasenia(String confirmarContrasenia) {
		this.confirmarContrasenia = confirmarContrasenia;
	}	
	
}
