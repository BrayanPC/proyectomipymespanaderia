package panaderiademo.controller.ventas;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import panaderiademo.controller.JSFUtil;
import panaderiademo.model.core.entities.EcoCliente;
import panaderiademo.model.core.entities.InvCategoria;
import panaderiademo.model.core.entities.InvProducto;
import panaderiademo.model.core.entities.VenDetalleVenta;
import panaderiademo.model.core.entities.VenImpuesto;
import panaderiademo.model.core.entities.VenVenta;
import panaderiademo.model.core.utils.ModelUtil;
import panaderiademo.model.ventas.dtos.ClienteDTO;
import panaderiademo.model.ventas.dtos.DetalleVentaDTO;
import panaderiademo.model.ventas.dtos.UsuarioDTO;
import panaderiademo.model.ventas.dtos.VentaDTO;
import panaderiademo.model.ventas.managers.ManagerVentas;

@Named
@SessionScoped
public class BeanVentas implements Serializable {
	private static final long serialVersionUID = 1L;
	@EJB
	private ManagerVentas managerVentas;
	private List<VenVenta> listaVentas;
	private List<DetalleVentaDTO> listaDetalleVenta;
	private List<InvCategoria> listaCategorias;
	private List<InvProducto> listaProductos;
	private int idclienteSeleccionado;
	private int idCatSeleccionada;
	private int idProSeleccionado;
	private int cantidad;
	private ClienteDTO clienteDTO;
	private UsuarioDTO usuarioDTO;
	private String mensaje;
	private String cedula;
	private boolean consumidorF;
	private EcoCliente clienteR;
	private VentaDTO venta;
	private String nombreP;
	private VenImpuesto impuesto;
	// Variables para pagina facturas
	private Date fechaInicio;
	private Date fechaFin;

	public BeanVentas() {
	}

	@PostConstruct
	public void inicializar() {

	}

	public void actionListenerAgregarDetalleVenta() {
		DetalleVentaDTO detalleVenta = new DetalleVentaDTO();
		detalleVenta.setIdPro(idProSeleccionado);
		InvProducto pro = new InvProducto();
		try {
			pro = managerVentas.findProductoById(idProSeleccionado);
			int id = 1;
			if (listaDetalleVenta.size() != 0) {
				id = listaDetalleVenta.get(listaDetalleVenta.size() - 1).getIdDetVen() + 1;
			}
			detalleVenta.setIdDetVen(id);
			detalleVenta.setNombrePro(pro.getNombrePro());
			detalleVenta.setCantidad(cantidad);
			double precio = pro.getPrecioPro().doubleValue();
			detalleVenta.setPrecio(precio);
			double total = cantidad * precio;
			detalleVenta.setTotal(total);
			listaDetalleVenta.add(detalleVenta);
			venta = managerVentas.calcularTotal(listaDetalleVenta, impuesto.getValorImp().doubleValue());
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
		}
	}

	public void actionListenerBuscarCliente() {
		if (cedula.length() > 0) {
			try {
				clienteDTO = managerVentas.findClienteByCedula(cedula);
				mensaje = "Nombres Cliente: " + clienteDTO.getNombres();
				consumidorF = false;
			} catch (Exception e) {
				mensaje = "";
				JSFUtil.crearMensajeERROR("Cliente No Registrado");
			}
		} else {
			mensaje = "";
			JSFUtil.crearMensajeERROR("Ingrese cedula");
		}
	}

	public void actionListenerModal() {
		clienteR = new EcoCliente();
	}

	public String actionListenerFinalizarVenta(int idUsu) {
		Date fecha = new Date();
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/YYYY");
		String fechaActual = format.format(fecha);
		if (listaDetalleVenta.size() > 0) {
			if (consumidorF) {
				try {
					clienteDTO = managerVentas.findClienteById(1);
					venta.setIdCLi(1);
					venta.setIdUsu(idUsu);
					venta.setFecha(fechaActual);
					venta.setIva(impuesto.getPorcentajeImp());
					usuarioDTO = managerVentas.findUsuarioById(idUsu);
					venta = managerVentas.registrarVenta(venta, listaDetalleVenta);
					System.out.println(venta.getIdVen());
				} catch (Exception e) {
					JSFUtil.crearMensajeERROR(e.getMessage());
				}
			} else {
				if (clienteDTO.getIdCli() > 0) {
					try {
						venta.setIdCLi(clienteDTO.getIdCli());
						venta.setIdUsu(idUsu);
						venta.setFecha(fechaActual);
						venta.setIva(impuesto.getPorcentajeImp());
						usuarioDTO = managerVentas.findUsuarioById(idUsu);
						venta = managerVentas.registrarVenta(venta, listaDetalleVenta);
						System.out.println(venta.getIdVen());
					} catch (Exception e) {
						JSFUtil.crearMensajeERROR(e.getMessage());
					}
				} else {
					JSFUtil.crearMensajeERROR("Ingrese Cliente.");
					return "";
				}
			}
		} else {
			JSFUtil.crearMensajeERROR("Lista de Venta Vacia.");
			return "";
		}
		return "generar_factura";
	}

	public void actionListenerRegistrarCliente() {
		if (clienteR.getCedulaCli().equals("")) {
			JSFUtil.crearMensajeERROR("Ingrese cedula");
		} else {
			try {
				clienteDTO = managerVentas.registrarCliente(clienteR);
				mensaje = "Nombres Cliente: " + clienteDTO.getNombres();
				consumidorF = false;
				JSFUtil.crearMensajeINFO("Cliente Registrado Exitosamente");
			} catch (Exception e) {
				JSFUtil.crearMensajeERROR(e.getMessage());
			}
		}
	}

	public void buscarCategoria() {
		listaProductos = managerVentas.findProductosByCatId(idCatSeleccionada, nombreP);
		if (listaProductos.size() < 1) {
			JSFUtil.crearMensajeERROR("0 Productos Econtrados");
		} else {
			JSFUtil.crearMensajeINFO(listaProductos.size() + " Productos Econtrados");
		}
	}

	public String actionListenerVerDetalles(int idVen) {
		try {
			VenVenta ven = managerVentas.findVentaById(idVen);
			clienteDTO = new ClienteDTO();
			clienteDTO.setIdCli(ven.getEcoCliente().getIdEcoCliente());
			clienteDTO.setCedula(ven.getEcoCliente().getCedulaCli());
			clienteDTO.setNombres(ven.getEcoCliente().getNombreCli() + " " + ven.getEcoCliente().getApellidoCli());
			usuarioDTO = new UsuarioDTO();
			usuarioDTO.setIdUsu(ven.getSegUsuario().getIdSegUsuario());
			usuarioDTO.setNombres(ven.getSegUsuario().getNombres() + " " + ven.getSegUsuario().getApellidos());
			venta = new VentaDTO();
			venta.setIdVen(ven.getIdVenVenta());
			venta.setFecha(ven.getFechaVen().toString());
			venta.setIva(ven.getIvaVen());
			venta.setSubtotal(ven.getSubtotalVen().doubleValue());
			venta.setImpuesto(ven.getImpuestoVen().doubleValue());
			venta.setTotal(ven.getTotalVen().doubleValue());
			listaDetalleVenta = new ArrayList<DetalleVentaDTO>();
			listaDetalleVenta = managerVentas.findDetalleVentasByVenId(idVen);
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
		}
		return "generar_factura";
	}

	public void actionListenerEliminarProductoDetalleVenta(int idDetVen) {
		listaDetalleVenta = managerVentas.eliminarProductoDetalleVenta(listaDetalleVenta, idDetVen);
		venta = managerVentas.calcularTotal(listaDetalleVenta, impuesto.getValorImp().doubleValue());
	}

	public String actionCargarMenuVender() {
//		// obtener la fecha de ayer:
//		fechaInicio = ModelUtil.addDays(new Date(), -1);
//		// obtener la fecha de hoy:
//		fechaFin = new Date();
//		listaBitacora = managerAuditoria.findBitacoraByFecha(fechaInicio, fechaFin);
//		JSFUtil.crearMensajeINFO("Registros encontrados: " + listaBitacora.size());
		consumidorF = true;
		listaCategorias = managerVentas.findAllCategorias();
		venta = new VentaDTO();
		listaProductos = new ArrayList<InvProducto>();
		listaDetalleVenta = new ArrayList<DetalleVentaDTO>();
		clienteDTO = new ClienteDTO();
		nombreP = "";
		mensaje = "";
		cedula = "";
		impuesto = new VenImpuesto();
		try {
			impuesto = managerVentas.findImpuesto();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "vender";
	}
	
	public void actionListenerCambiarIva() {
		try {
			impuesto = managerVentas.findImpuesto();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void actionListenerActualizarImpuesto(){
		try {
			impuesto = managerVentas.actualizarImpuesto(impuesto);
			venta = managerVentas.calcularTotal(listaDetalleVenta, impuesto.getValorImp().doubleValue());
			JSFUtil.crearMensajeINFO("Impuesto Actualizado");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String actionCargarMenuVentas() {
		// obtener la fecha de ayer:
		fechaInicio = ModelUtil.addDays(new Date(), -1);
		// obtener la fecha de hoy:
		fechaFin = new Date();
		listaVentas = managerVentas.findVentasByFecha(fechaInicio, fechaFin);
		JSFUtil.crearMensajeINFO("Registros encontrados: " + listaVentas.size());
		return "ventas";
	}

	public void actionListenerEliminarVenta(int idVen) {
		try {
			managerVentas.eliminarVenta(idVen);
			JSFUtil.crearMensajeINFO("Venta Eliminada.");
			listaVentas = managerVentas.findVentasByFecha(fechaInicio, fechaFin);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			JSFUtil.crearMensajeERROR(e.getMessage());
		}
	}

	public String actionReporte() {
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("idVen", venta.getIdVen());
		FacesContext context = FacesContext.getCurrentInstance();
		ServletContext servletContext = (ServletContext) context.getExternalContext().getContext();
		String ruta = servletContext.getRealPath("ventas/factura.jasper");
		System.out.println(ruta);
		HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
		response.addHeader("Content-disposition", "attachment;filename=reporte.pdf");
		response.setContentType("application/pdf");
		try {
			Class.forName("org.postgresql.Driver");
			Connection connection = null;
			connection = DriverManager.getConnection("jdbc:postgresql://192.100.198.141:5432/bapulamarinc",
					"bapulamarinc", "1727468512");
			JasperPrint impresion = JasperFillManager.fillReport(ruta, parametros, connection);
			JasperExportManager.exportReportToPdfStream(impresion, response.getOutputStream());
			context.getApplication().getStateManager().saveView(context);
			System.out.println("reporte generado.");
			context.responseComplete();
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
		return "";
	}

	public void actionListenerBuscarVentas() {
		listaVentas = managerVentas.findVentasByFecha(fechaInicio, fechaFin);
	}

	public List<VenVenta> getListaVentas() {
		return listaVentas;
	}

	public void setListaVentas(List<VenVenta> listaVentas) {
		this.listaVentas = listaVentas;
	}

	public List<DetalleVentaDTO> getListaDetalleVenta() {
		return listaDetalleVenta;
	}

	public ClienteDTO getClienteDTO() {
		return clienteDTO;
	}

	public void setClienteDTO(ClienteDTO clienteDTO) {
		this.clienteDTO = clienteDTO;
	}

	public UsuarioDTO getUsuarioDTO() {
		return usuarioDTO;
	}

	public void setUsuarioDTO(UsuarioDTO usuarioDTO) {
		this.usuarioDTO = usuarioDTO;
	}

	public void setListaDetalleVenta(List<DetalleVentaDTO> listaDetalleVenta) {
		this.listaDetalleVenta = listaDetalleVenta;
	}

	public List<InvCategoria> getListaCategorias() {
		return listaCategorias;
	}

	public void setListaCategorias(List<InvCategoria> listaCategorias) {
		this.listaCategorias = listaCategorias;
	}

	public List<InvProducto> getListaProductos() {
		return listaProductos;
	}

	public void setListaProductos(List<InvProducto> listaProductos) {
		this.listaProductos = listaProductos;
	}

	public int getIdclienteSeleccionado() {
		return idclienteSeleccionado;
	}

	public void setIdclienteSeleccionado(int idclienteSeleccionado) {
		this.idclienteSeleccionado = idclienteSeleccionado;
	}

	public int getIdCatSeleccionada() {
		return idCatSeleccionada;
	}

	public void setIdCatSeleccionada(int idCatSeleccionada) {
		this.idCatSeleccionada = idCatSeleccionada;
	}

	public int getIdProSeleccionado() {
		return idProSeleccionado;
	}

	public void setIdProSeleccionado(int idProSeleccionado) {
		this.idProSeleccionado = idProSeleccionado;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public String getCedula() {
		return cedula;
	}

	public void setCedula(String cedula) {
		this.cedula = cedula;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public boolean isConsumidorF() {
		return consumidorF;
	}

	public void setConsumidorF(boolean consumidorF) {
		this.consumidorF = consumidorF;
	}

	public EcoCliente getClienteR() {
		return clienteR;
	}

	public void setClienteR(EcoCliente clienteR) {
		this.clienteR = clienteR;
	}

	public VentaDTO getVenta() {
		return venta;
	}

	public void setVenta(VentaDTO venta) {
		this.venta = venta;
	}

	public Date getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public Date getFechaFin() {
		return fechaFin;
	}

	public String getNombreP() {
		return nombreP;
	}

	public void setNombreP(String nombreP) {
		this.nombreP = nombreP;
	}

	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}

	public VenImpuesto getImpuesto() {
		return impuesto;
	}

	public void setImpuesto(VenImpuesto impuesto) {
		this.impuesto = impuesto;
	}
	
}
