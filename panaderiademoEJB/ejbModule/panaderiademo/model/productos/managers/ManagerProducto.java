package panaderiademo.model.productos.managers;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import panaderiademo.model.core.entities.InvCategoria;
import panaderiademo.model.core.entities.InvProducto;

/**
 * Session Bean implementation class ManagerProducto
 */
@Stateless
@LocalBean
public class ManagerProducto {
	@PersistenceContext
	
	private EntityManager em;

	public ManagerProducto() {
	}
	
	

	public List<InvProducto> findAllProductos() {
		return em.createNamedQuery("InvProducto.findAll", InvProducto.class).getResultList();
	}
	
	public List<InvCategoria> findAllCategorias() {
		return em.createNamedQuery("InvCategoria.findAll", InvCategoria.class).getResultList();
	}

	public void eliminarProducto(int idProducto) throws Exception {
		InvProducto producto = em.find(InvProducto.class, idProducto);
		if (producto == null)
			throw new Exception("No existe el Producto especificado: " + idProducto);
		em.remove(producto);

	}
	
	public InvProducto actualizarProducto(InvProducto productoEdicion,int idCatProducto) {
		InvProducto p=em.find(InvProducto.class, productoEdicion.getIdInvProducto());
    	p.setNombrePro(productoEdicion.getNombrePro());
    	p.setCantidadPro(productoEdicion.getCantidadPro());
    	p.setPrecioPro(productoEdicion.getPrecioPro());
    	p.setEstadoPro(productoEdicion.getEstadoPro());
    	p.setUrlImagenPro(productoEdicion.getUrlImagenPro());
    	InvCategoria c=em.find(InvCategoria.class, idCatProducto);
    	p.setInvCategoria(c);
    	em.merge(p);
    	return p;
    }


	public void insertarProducto(InvProducto producto, int idCat) throws Exception{
		InvCategoria categoria = em.find(InvCategoria.class, idCat);
		producto.setInvCategoria(categoria);
		em.persist(producto);
	}
	
	public void eliminarCategoria(int idCategoria) throws Exception {
		InvCategoria categoria = em.find(InvCategoria.class, idCategoria);
		if (categoria == null)
			throw new Exception("No existe el Producto especificado: " + idCategoria);
		em.remove(categoria);

	}
	
	public InvCategoria actualizarCategoria(InvCategoria categoriaEdicion) {
		InvCategoria c=em.find(InvCategoria.class, categoriaEdicion.getIdInvCategoria());
    	c.setNombreCat(categoriaEdicion.getNombreCat());
    	c.setDescripcionCat(categoriaEdicion.getDescripcionCat());
    	em.merge(c);
    	return c;
    }


	public void insertarCategoria(InvCategoria categoria) throws Exception{
		em.persist(categoria);
	}
	
	public InvProducto cambiarEstadoDisponible(InvProducto producto) {
		producto.setEstadoPro(!producto.getEstadoPro());
    	em.merge(producto);
    	return producto;
    }
	
	
	
	
	

}
