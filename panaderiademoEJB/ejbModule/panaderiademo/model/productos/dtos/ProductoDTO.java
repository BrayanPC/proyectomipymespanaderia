package panaderiademo.model.productos.dtos;

public class ProductoDTO {
	private int idPro;
	private String nombrePro;
	private String urlImg;
	private double precioPro;
	private int cantidad;
	
	public ProductoDTO() {
		this.idPro = 0;
		this.nombrePro = "";
		this.urlImg = "";
		this.precioPro = 0;
		this.cantidad = 0;
	}

	public ProductoDTO(int idPro, String nombrePro, String urlImg, double precioPro, int cantidad) {
		this.idPro = idPro;
		this.nombrePro = nombrePro;
		this.urlImg = urlImg;
		this.precioPro = precioPro;
		this.cantidad = cantidad;
	}

	public int getIdPro() {
		return idPro;
	}

	public void setIdPro(int idPro) {
		this.idPro = idPro;
	}

	public String getNombrePro() {
		return nombrePro;
	}

	public void setNombrePro(String nombrePro) {
		this.nombrePro = nombrePro;
	}

	public String getUrlImg() {
		return urlImg;
	}

	public void setUrlImg(String urlImg) {
		this.urlImg = urlImg;
	}

	public double getPrecioPro() {
		return precioPro;
	}

	public void setPrecioPro(double precioPro) {
		this.precioPro = precioPro;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

}
