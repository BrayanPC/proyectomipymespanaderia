package panaderiademo.model.ventas.managers;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;

import panaderiademo.model.core.entities.EcoCliente;
import panaderiademo.model.core.entities.InvCategoria;
import panaderiademo.model.core.entities.InvProducto;
import panaderiademo.model.core.entities.SegUsuario;
import panaderiademo.model.core.entities.VenDetalleVenta;
import panaderiademo.model.core.entities.VenImpuesto;
import panaderiademo.model.core.entities.VenVenta;
import panaderiademo.model.core.managers.ManagerDAO;
import panaderiademo.model.ventas.dtos.ClienteDTO;
import panaderiademo.model.ventas.dtos.DetalleVentaDTO;
import panaderiademo.model.ventas.dtos.UsuarioDTO;
import panaderiademo.model.ventas.dtos.VentaDTO;

/**
 * Session Bean implementation class ManagerVentas
 */
@Stateless
@LocalBean
public class ManagerVentas {
	@EJB
	private ManagerDAO mDAO;

	public ManagerVentas() {
		// TODO Auto-generated constructor stub
	}
	
	public ClienteDTO findClienteById(int idCli) throws Exception {
		EcoCliente ecoCliente = new EcoCliente();
		ClienteDTO cliente = new ClienteDTO();
		try {
			ecoCliente = (EcoCliente) mDAO.findById(EcoCliente.class, idCli);
			cliente.setIdCli(ecoCliente.getIdEcoCliente());
			cliente.setNombres(ecoCliente.getNombreCli() + " " + ecoCliente.getApellidoCli());
			cliente.setCedula(ecoCliente.getCedulaCli());
		} catch (Exception e) {
			throw new Exception("Cliente No Registrado");
		}
		return cliente;
	}

	public ClienteDTO findClienteByCedula(String cedula) throws Exception {
		String clausula = "cedula_cli = '" + cedula + "'";
		EcoCliente ecoCliente = new EcoCliente();
		ClienteDTO cliente = new ClienteDTO();
		try {
			ecoCliente = (EcoCliente) mDAO.findWhereObject(EcoCliente.class, clausula);
			cliente.setIdCli(ecoCliente.getIdEcoCliente());
			cliente.setNombres(ecoCliente.getNombreCli() + " " + ecoCliente.getApellidoCli());
			cliente.setCedula(ecoCliente.getCedulaCli());
		} catch (Exception e) {
			throw new Exception("Cliente No Registrado");
		}
		return cliente;
	}

	public List<InvCategoria> findAllCategorias() {
		List<InvCategoria> listaCategorias = new ArrayList<InvCategoria>();
		listaCategorias = mDAO.findAll(InvCategoria.class);
		InvCategoria categoria = new InvCategoria();
		categoria.setIdInvCategoria(0);
		categoria.setNombreCat("Todas");
		categoria.setDescripcionCat("");
		listaCategorias.add(categoria);
		return listaCategorias;
	}
	
	public VenImpuesto findImpuesto() throws Exception {
		VenImpuesto impuesto = (VenImpuesto) mDAO.findById(VenImpuesto.class, 1);
		return impuesto;
	}

	public VenVenta findVentaById(int idVen) throws Exception {
		VenVenta venta = (VenVenta) mDAO.findById(VenVenta.class, idVen);
		if (venta == null) {
			throw new Exception("Venta no encontrada.");
		}
		return venta;
	}

	public List<VenVenta> findVentasByFecha(Date fechaInicio, Date fechaFin) {
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		String consulta = "select v from VenVenta v where v.fechaVen between :fechaInicio and :fechaFin order by v.fechaVen";
		Query q = mDAO.getEntityManager().createQuery(consulta, VenVenta.class);
		q.setParameter("fechaInicio", new Timestamp(fechaInicio.getTime()));
		q.setParameter("fechaFin", new Timestamp(fechaFin.getTime()));
		return q.getResultList();
	}

	public List<DetalleVentaDTO> findDetalleVentasByVenId(int idVen) {
		String clausula = "id_ven_venta = " + idVen;
		List<VenDetalleVenta> listaDetalleVentas = mDAO.findWhere(VenDetalleVenta.class, clausula, "");
		List<DetalleVentaDTO> lista = new ArrayList<DetalleVentaDTO>();
		int i = 1;
		for (VenDetalleVenta detVen : listaDetalleVentas) {
			DetalleVentaDTO detVenDTO = new DetalleVentaDTO();
			detVenDTO.setIdDetVen(i);
			detVenDTO.setIdPro(detVen.getInvProducto().getIdInvProducto());
			detVenDTO.setNombrePro(detVen.getInvProducto().getNombrePro());
			detVenDTO.setCantidad(detVen.getCantidadVen());
			detVenDTO.setPrecio(detVen.getPrecioVen().doubleValue());
			detVenDTO.setTotal(detVen.getTotalVen().doubleValue());
			lista.add(detVenDTO);
			i++;
		}
		return lista;
	}

	public List<InvProducto> findProductosByCatId(int idCat, String nombreP) {
		List<InvProducto> listaProductos = new ArrayList<InvProducto>();
		String clausula = "";
		if (idCat == 0) {
			if (nombreP.length() > 0) {
				clausula = "nombre_pro LIKE '" + nombreP + "%' or nombre_pro LIKE '%" + nombreP
						+ "%' or nombre_pro LIKE '%" + nombreP + "'";
				listaProductos = mDAO.findWhere(InvProducto.class, clausula, "");
				return listaProductos;
			} else {
				listaProductos = mDAO.findAll(InvProducto.class);
				return listaProductos;
			}
		}
		if (nombreP.length() > 0) {
			clausula = "nombre_pro LIKE '" + nombreP + "%' or nombre_pro LIKE '%" + nombreP + "%' or nombre_pro LIKE '%"
					+ nombreP + "' and id_inv_categoria = " + idCat;
			listaProductos = mDAO.findWhere(InvProducto.class, clausula, "");
		} else {
			clausula = "id_inv_categoria = " + idCat;
			listaProductos = mDAO.findWhere(InvProducto.class, clausula, "");
		}
		return listaProductos;
	}

	public InvProducto findProductoById(int idPro) throws Exception {
		InvProducto pro = (InvProducto) mDAO.findById(InvProducto.class, idPro);
		if (pro == null) {
			throw new Exception("Seleccione Un Producto.");
		}
		return pro;
	}

	public UsuarioDTO findUsuarioById(int idUsu) throws Exception {
		SegUsuario usu = (SegUsuario) mDAO.findById(SegUsuario.class, idUsu);
		if (usu == null) {
			throw new Exception("Usuario no encontrado.");
		}
		UsuarioDTO usuarioDTO = new UsuarioDTO();
		usuarioDTO.setIdUsu(usu.getIdSegUsuario());
		usuarioDTO.setNombres(usu.getNombres() + " " + usu.getApellidos());
		return usuarioDTO;
	}

	public ClienteDTO registrarCliente(EcoCliente cliente) throws Exception {
		cliente.setClaveCli("");
		mDAO.insertar(cliente);
		ClienteDTO cli = new ClienteDTO();
		cli.setIdCli(cliente.getIdEcoCliente());
		cli.setNombres(cliente.getNombreCli() + " " + cliente.getApellidoCli());
		return cli;
	}
	
	public VenImpuesto actualizarImpuesto(VenImpuesto impuesto) throws Exception {
		mDAO.actualizar(impuesto);
		return impuesto;
	}

	public VentaDTO registrarVenta(VentaDTO ventaDTO, List<DetalleVentaDTO> listaDetalleVenta) throws Exception {
		VenVenta venta = new VenVenta();
		EcoCliente cliente = new EcoCliente();
		if (ventaDTO.getIdCLi() != 0) {
			cliente = (EcoCliente) mDAO.findById(EcoCliente.class, ventaDTO.getIdCLi());
			venta.setEcoCliente(cliente);
		}
		SegUsuario usuario = (SegUsuario) mDAO.findById(SegUsuario.class, ventaDTO.getIdUsu());
		venta.setSegUsuario(usuario);
		venta.setFechaVen(new Date());
		venta.setIvaVen(ventaDTO.getIva());
		venta.setSubtotalVen(new BigDecimal(ventaDTO.getSubtotal()));
		venta.setImpuestoVen(new BigDecimal(ventaDTO.getImpuesto()));
		venta.setTotalVen(new BigDecimal(ventaDTO.getTotal()));
		mDAO.insertar(venta);
		ventaDTO.setIdVen(venta.getIdVenVenta());
//		venta.setVenDetalleVentas(new ArrayList<VenDetalleVenta>());
		for (DetalleVentaDTO detVen : listaDetalleVenta) {
			VenDetalleVenta detalleVenta = new VenDetalleVenta();
			detalleVenta.setVenVenta(venta);
			InvProducto producto = (InvProducto) mDAO.findById(InvProducto.class, detVen.getIdPro());
			detalleVenta.setInvProducto(producto);
			detalleVenta.setCantidadVen(detVen.getCantidad());
			detalleVenta.setPrecioVen(new BigDecimal(detVen.getPrecio()));
			detalleVenta.setTotalVen(new BigDecimal(detVen.getTotal()));
			mDAO.insertar(detalleVenta);
//			venta.addVenDetalleVenta(detalleVenta);
		}

		return ventaDTO;
	}

	public List<DetalleVentaDTO> eliminarProductoDetalleVenta(List<DetalleVentaDTO> lista, int idDetVen) {
		if (lista == null)
			return null;
		int i = 0;
		for (DetalleVentaDTO detVen : lista) {
			if (detVen.getIdDetVen() == idDetVen) {
				lista.remove(i);
				break;
			}
			i++;
		}
		return lista;
	}

	public void eliminarVenta(int idDetVen) throws Exception {
		mDAO.eliminar(VenVenta.class, idDetVen);
	}

	public VentaDTO calcularTotal(List<DetalleVentaDTO> listado, double valorImp) {
		double subtotal = 0;
		for (DetalleVentaDTO detVen : listado)
			subtotal += detVen.getTotal();
		VentaDTO venta = new VentaDTO();
		venta.setSubtotal(subtotal);
		double impuesto = subtotal * valorImp;
		venta.setImpuesto(impuesto);
		double total = subtotal + impuesto;
		venta.setTotal(total);
		return venta;
	}
}
