package panaderiademo.model.ventas.dtos;

public class ClienteDTO {
	private int idCli;
	private String cedula;
	private String nombres;
	
	public ClienteDTO() {
		this.idCli = 0;
		this.nombres = "";
	}
	public ClienteDTO(int idCli, String nombres) {
		this.idCli = idCli;
		this.nombres = nombres;
	}
	public int getIdCli() {
		return idCli;
	}
	public void setIdCli(int idCli) {
		this.idCli = idCli;
	}
	public String getCedula() {
		return cedula;
	}
	public void setCedula(String cedula) {
		this.cedula = cedula;
	}
	public String getNombres() {
		return nombres;
	}
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

}
