package panaderiademo.model.ventas.dtos;

public class DetalleVentaDTO {
	private int idDetVen;
	private int idPro;
	private String nombrePro;
	private int cantidad;
	private double precio;
	private double total;
	
	public DetalleVentaDTO() {
		this.idDetVen = 0;
		this.idPro = 0;
		this.nombrePro = "";
		this.cantidad = 0;
		this.precio = 0;
		this.total = 0;
	}
	public DetalleVentaDTO(int idDetVen, int idPro, String nombrePro, int cantidad, double precio, double total) {
		this.idDetVen = idDetVen;
		this.idPro = idPro;
		this.nombrePro = nombrePro;
		this.cantidad = cantidad;
		this.precio = precio;
		this.total = total;
	}
	public int getIdDetVen() {
		return idDetVen;
	}
	public void setIdDetVen(int idDetVen) {
		this.idDetVen = idDetVen;
	}
	public int getIdPro() {
		return idPro;
	}
	public void setIdPro(int idPro) {
		this.idPro = idPro;
	}
	public String getNombrePro() {
		return nombrePro;
	}
	public void setNombrePro(String nombrePro) {
		this.nombrePro = nombrePro;
	}
	public int getCantidad() {
		return cantidad;
	}
	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}
	public double getPrecio() {
		return precio;
	}
	public void setPrecio(double precio) {
		this.precio = precio;
	}
	public double getTotal() {
		return total;
	}
	public void setTotal(double total) {
		this.total = total;
	}

}
