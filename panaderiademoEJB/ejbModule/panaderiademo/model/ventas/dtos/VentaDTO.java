package panaderiademo.model.ventas.dtos;

public class VentaDTO {
	private int idVen;
	private int idCLi;
	private String nombresCli;
	private int idUsu;
	private String iva;
	private String nombresUsu;
	private String fecha;
	private double subtotal;
	private double impuesto;
	private double total;

	public VentaDTO() {
		this.idVen = 0;
		this.idCLi = 0;
		this.nombresCli = "";
		this.idUsu = 0;
		this.iva = "";
		this.nombresUsu = "";
		this.fecha = "";
		this.subtotal = 0;
		this.impuesto = 0;
		this.total = 0;
	}

	public VentaDTO(int idVen, int idCLi, String nombresCli, int idUsu, String iva, String nombresUsu, String fecha,
			double subtotal, double impuesto, double total) {
		this.idVen = idVen;
		this.idCLi = idCLi;
		this.nombresCli = nombresCli;
		this.idUsu = idUsu;
		this.iva = iva;
		this.nombresUsu = nombresUsu;
		this.fecha = fecha;
		this.subtotal = subtotal;
		this.impuesto = impuesto;
		this.total = total;
	}

	public String getIva() {
		return iva;
	}

	public void setIva(String iva) {
		this.iva = iva;
	}

	public int getIdVen() {
		return idVen;
	}

	public void setIdVen(int idVen) {
		this.idVen = idVen;
	}

	public int getIdCLi() {
		return idCLi;
	}

	public void setIdCLi(int idCLi) {
		this.idCLi = idCLi;
	}

	public String getNombresCli() {
		return nombresCli;
	}

	public void setNombresCli(String nombresCli) {
		this.nombresCli = nombresCli;
	}

	public int getIdUsu() {
		return idUsu;
	}

	public void setIdUsu(int idUsu) {
		this.idUsu = idUsu;
	}

	public String getNombresUsu() {
		return nombresUsu;
	}

	public void setNombresUsu(String nombresUsu) {
		this.nombresUsu = nombresUsu;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public double getSubtotal() {
		return subtotal;
	}

	public void setSubtotal(double subtotal) {
		this.subtotal = subtotal;
	}

	public double getImpuesto() {
		return impuesto;
	}

	public void setImpuesto(double impuesto) {
		this.impuesto = impuesto;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}
}
