package panaderiademo.model.ventas.dtos;

public class UsuarioDTO {
	private int idUsu;
	private String nombres;
	public int getIdUsu() {
		return idUsu;
	}
	public void setIdUsu(int idUsu) {
		this.idUsu = idUsu;
	}
	public String getNombres() {
		return nombres;
	}
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

}
