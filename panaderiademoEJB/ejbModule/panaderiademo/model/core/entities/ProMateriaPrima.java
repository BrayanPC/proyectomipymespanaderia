package panaderiademo.model.core.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the pro_materia_prima database table.
 * 
 */
@Entity
@Table(name="pro_materia_prima")
@NamedQuery(name="ProMateriaPrima.findAll", query="SELECT p FROM ProMateriaPrima p")
public class ProMateriaPrima implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_pro_materia_prima", unique=true, nullable=false)
	private Integer idProMateriaPrima;

	@Column(name="cantidad_mat", nullable=false, precision=5, scale=2)
	private BigDecimal cantidadMat;

	@Column(name="estado_mat", nullable=false)
	private Boolean estadoMat;

	@Column(name="nombre_mat", nullable=false, length=1024)
	private String nombreMat;

	@Column(name="precio_mat", precision=5, scale=2)
	private BigDecimal precioMat;

	//bi-directional many-to-one association to ProMedida
	@ManyToOne
	@JoinColumn(name="id_pro_medida")
	private ProMedida proMedida;

	public ProMateriaPrima() {
	}

	public Integer getIdProMateriaPrima() {
		return this.idProMateriaPrima;
	}

	public void setIdProMateriaPrima(Integer idProMateriaPrima) {
		this.idProMateriaPrima = idProMateriaPrima;
	}

	public BigDecimal getCantidadMat() {
		return this.cantidadMat;
	}

	public void setCantidadMat(BigDecimal cantidadMat) {
		this.cantidadMat = cantidadMat;
	}

	public Boolean getEstadoMat() {
		return this.estadoMat;
	}

	public void setEstadoMat(Boolean estadoMat) {
		this.estadoMat = estadoMat;
	}

	public String getNombreMat() {
		return this.nombreMat;
	}

	public void setNombreMat(String nombreMat) {
		this.nombreMat = nombreMat;
	}

	public BigDecimal getPrecioMat() {
		return this.precioMat;
	}

	public void setPrecioMat(BigDecimal precioMat) {
		this.precioMat = precioMat;
	}

	public ProMedida getProMedida() {
		return this.proMedida;
	}

	public void setProMedida(ProMedida proMedida) {
		this.proMedida = proMedida;
	}

}