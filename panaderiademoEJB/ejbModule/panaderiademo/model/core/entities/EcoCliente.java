package panaderiademo.model.core.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the eco_cliente database table.
 * 
 */
@Entity
@Table(name="eco_cliente")
@NamedQuery(name="EcoCliente.findAll", query="SELECT e FROM EcoCliente e")
public class EcoCliente implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_eco_cliente", unique=true, nullable=false)
	private Integer idEcoCliente;

	@Column(name="apellido_cli", nullable=false, length=1024)
	private String apellidoCli;

	@Column(name="cedula_cli", nullable=false, length=1024)
	private String cedulaCli;

	@Column(name="clave_cli", nullable=false, length=1024)
	private String claveCli;

	@Column(name="correo_cli", nullable=false, length=1024)
	private String correoCli;

	@Column(name="nombre_cli", nullable=false, length=1024)
	private String nombreCli;

	@Column(name="telefono_cli", length=1024)
	private String telefonoCli;

	//bi-directional many-to-one association to VenVenta
	@OneToMany(mappedBy="ecoCliente")
	private List<VenVenta> venVentas;

	public EcoCliente() {
	}

	public Integer getIdEcoCliente() {
		return this.idEcoCliente;
	}

	public void setIdEcoCliente(Integer idEcoCliente) {
		this.idEcoCliente = idEcoCliente;
	}

	public String getApellidoCli() {
		return this.apellidoCli;
	}

	public void setApellidoCli(String apellidoCli) {
		this.apellidoCli = apellidoCli;
	}

	public String getCedulaCli() {
		return this.cedulaCli;
	}

	public void setCedulaCli(String cedulaCli) {
		this.cedulaCli = cedulaCli;
	}

	public String getClaveCli() {
		return this.claveCli;
	}

	public void setClaveCli(String claveCli) {
		this.claveCli = claveCli;
	}

	public String getCorreoCli() {
		return this.correoCli;
	}

	public void setCorreoCli(String correoCli) {
		this.correoCli = correoCli;
	}

	public String getNombreCli() {
		return this.nombreCli;
	}

	public void setNombreCli(String nombreCli) {
		this.nombreCli = nombreCli;
	}

	public String getTelefonoCli() {
		return this.telefonoCli;
	}

	public void setTelefonoCli(String telefonoCli) {
		this.telefonoCli = telefonoCli;
	}

	public List<VenVenta> getVenVentas() {
		return this.venVentas;
	}

	public void setVenVentas(List<VenVenta> venVentas) {
		this.venVentas = venVentas;
	}

	public VenVenta addVenVenta(VenVenta venVenta) {
		getVenVentas().add(venVenta);
		venVenta.setEcoCliente(this);

		return venVenta;
	}

	public VenVenta removeVenVenta(VenVenta venVenta) {
		getVenVentas().remove(venVenta);
		venVenta.setEcoCliente(null);

		return venVenta;
	}

}