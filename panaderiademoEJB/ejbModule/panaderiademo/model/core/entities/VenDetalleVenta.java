package panaderiademo.model.core.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the ven_detalle_venta database table.
 * 
 */
@Entity
@Table(name="ven_detalle_venta")
@NamedQuery(name="VenDetalleVenta.findAll", query="SELECT v FROM VenDetalleVenta v")
public class VenDetalleVenta implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_ven_detalle_venta", unique=true, nullable=false)
	private Integer idVenDetalleVenta;

	@Column(name="cantidad_ven", nullable=false)
	private Integer cantidadVen;

	@Column(name="precio_ven", nullable=false, precision=5, scale=2)
	private BigDecimal precioVen;

	@Column(name="total_ven", nullable=false, precision=5, scale=2)
	private BigDecimal totalVen;

	//bi-directional many-to-one association to InvProducto
	@ManyToOne
	@JoinColumn(name="id_inv_producto")
	private InvProducto invProducto;

	//bi-directional many-to-one association to VenVenta
	@ManyToOne
	@JoinColumn(name="id_ven_venta")
	private VenVenta venVenta;

	public VenDetalleVenta() {
	}

	public Integer getIdVenDetalleVenta() {
		return this.idVenDetalleVenta;
	}

	public void setIdVenDetalleVenta(Integer idVenDetalleVenta) {
		this.idVenDetalleVenta = idVenDetalleVenta;
	}

	public Integer getCantidadVen() {
		return this.cantidadVen;
	}

	public void setCantidadVen(Integer cantidadVen) {
		this.cantidadVen = cantidadVen;
	}

	public BigDecimal getPrecioVen() {
		return this.precioVen;
	}

	public void setPrecioVen(BigDecimal precioVen) {
		this.precioVen = precioVen;
	}

	public BigDecimal getTotalVen() {
		return this.totalVen;
	}

	public void setTotalVen(BigDecimal totalVen) {
		this.totalVen = totalVen;
	}

	public InvProducto getInvProducto() {
		return this.invProducto;
	}

	public void setInvProducto(InvProducto invProducto) {
		this.invProducto = invProducto;
	}

	public VenVenta getVenVenta() {
		return this.venVenta;
	}

	public void setVenVenta(VenVenta venVenta) {
		this.venVenta = venVenta;
	}

}