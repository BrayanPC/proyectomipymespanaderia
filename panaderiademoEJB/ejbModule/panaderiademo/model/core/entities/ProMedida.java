package panaderiademo.model.core.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the pro_medida database table.
 * 
 */
@Entity
@Table(name="pro_medida")
@NamedQuery(name="ProMedida.findAll", query="SELECT p FROM ProMedida p")
public class ProMedida implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_pro_medida", unique=true, nullable=false)
	private Integer idProMedida;

	@Column(name="abreviatura_med", nullable=false, length=4)
	private String abreviaturaMed;

	@Column(name="nombre_med", nullable=false, length=1024)
	private String nombreMed;

	//bi-directional many-to-one association to ProMateriaPrima
	@OneToMany(mappedBy="proMedida")
	private List<ProMateriaPrima> proMateriaPrimas;

	public ProMedida() {
	}

	public Integer getIdProMedida() {
		return this.idProMedida;
	}

	public void setIdProMedida(Integer idProMedida) {
		this.idProMedida = idProMedida;
	}

	public String getAbreviaturaMed() {
		return this.abreviaturaMed;
	}

	public void setAbreviaturaMed(String abreviaturaMed) {
		this.abreviaturaMed = abreviaturaMed;
	}

	public String getNombreMed() {
		return this.nombreMed;
	}

	public void setNombreMed(String nombreMed) {
		this.nombreMed = nombreMed;
	}

	public List<ProMateriaPrima> getProMateriaPrimas() {
		return this.proMateriaPrimas;
	}

	public void setProMateriaPrimas(List<ProMateriaPrima> proMateriaPrimas) {
		this.proMateriaPrimas = proMateriaPrimas;
	}

	public ProMateriaPrima addProMateriaPrima(ProMateriaPrima proMateriaPrima) {
		getProMateriaPrimas().add(proMateriaPrima);
		proMateriaPrima.setProMedida(this);

		return proMateriaPrima;
	}

	public ProMateriaPrima removeProMateriaPrima(ProMateriaPrima proMateriaPrima) {
		getProMateriaPrimas().remove(proMateriaPrima);
		proMateriaPrima.setProMedida(null);

		return proMateriaPrima;
	}

}