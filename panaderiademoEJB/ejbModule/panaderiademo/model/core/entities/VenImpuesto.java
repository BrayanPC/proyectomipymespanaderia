package panaderiademo.model.core.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the ven_impuesto database table.
 * 
 */
@Entity
@Table(name="ven_impuesto")
@NamedQuery(name="VenImpuesto.findAll", query="SELECT v FROM VenImpuesto v")
public class VenImpuesto implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_ven_impuesto", unique=true, nullable=false)
	private Integer idVenImpuesto;

	@Column(name="nombre_imp", nullable=false, length=1024)
	private String nombreImp;

	@Column(name="porcentaje_imp", length=10)
	private String porcentajeImp;

	@Column(name="valor_imp", nullable=false, precision=7, scale=2)
	private BigDecimal valorImp;

	public VenImpuesto() {
	}

	public Integer getIdVenImpuesto() {
		return this.idVenImpuesto;
	}

	public void setIdVenImpuesto(Integer idVenImpuesto) {
		this.idVenImpuesto = idVenImpuesto;
	}

	public String getNombreImp() {
		return this.nombreImp;
	}

	public void setNombreImp(String nombreImp) {
		this.nombreImp = nombreImp;
	}

	public String getPorcentajeImp() {
		return this.porcentajeImp;
	}

	public void setPorcentajeImp(String porcentajeImp) {
		this.porcentajeImp = porcentajeImp;
	}

	public BigDecimal getValorImp() {
		return this.valorImp;
	}

	public void setValorImp(BigDecimal valorImp) {
		this.valorImp = valorImp;
	}

}