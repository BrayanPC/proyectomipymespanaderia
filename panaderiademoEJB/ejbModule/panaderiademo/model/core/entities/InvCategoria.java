package panaderiademo.model.core.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the inv_categoria database table.
 * 
 */
@Entity
@Table(name="inv_categoria")
@NamedQuery(name="InvCategoria.findAll", query="SELECT i FROM InvCategoria i")
public class InvCategoria implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_inv_categoria", unique=true, nullable=false)
	private Integer idInvCategoria;

	@Column(name="descripcion_cat", length=1024)
	private String descripcionCat;

	@Column(name="nombre_cat", nullable=false, length=1024)
	private String nombreCat;

	//bi-directional many-to-one association to InvProducto
	@OneToMany(mappedBy="invCategoria")
	private List<InvProducto> invProductos;

	public InvCategoria() {
	}

	public Integer getIdInvCategoria() {
		return this.idInvCategoria;
	}

	public void setIdInvCategoria(Integer idInvCategoria) {
		this.idInvCategoria = idInvCategoria;
	}

	public String getDescripcionCat() {
		return this.descripcionCat;
	}

	public void setDescripcionCat(String descripcionCat) {
		this.descripcionCat = descripcionCat;
	}

	public String getNombreCat() {
		return this.nombreCat;
	}

	public void setNombreCat(String nombreCat) {
		this.nombreCat = nombreCat;
	}

	public List<InvProducto> getInvProductos() {
		return this.invProductos;
	}

	public void setInvProductos(List<InvProducto> invProductos) {
		this.invProductos = invProductos;
	}

	public InvProducto addInvProducto(InvProducto invProducto) {
		getInvProductos().add(invProducto);
		invProducto.setInvCategoria(this);

		return invProducto;
	}

	public InvProducto removeInvProducto(InvProducto invProducto) {
		getInvProductos().remove(invProducto);
		invProducto.setInvCategoria(null);

		return invProducto;
	}

}