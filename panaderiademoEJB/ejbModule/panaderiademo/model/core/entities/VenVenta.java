package panaderiademo.model.core.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the ven_venta database table.
 * 
 */
@Entity
@Table(name="ven_venta")
@NamedQuery(name="VenVenta.findAll", query="SELECT v FROM VenVenta v")
public class VenVenta implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_ven_venta", unique=true, nullable=false)
	private Integer idVenVenta;

	@Temporal(TemporalType.DATE)
	@Column(name="fecha_ven", nullable=false)
	private Date fechaVen;

	@Column(name="impuesto_ven", nullable=false, precision=5, scale=2)
	private BigDecimal impuestoVen;

	@Column(name="iva_ven", length=10)
	private String ivaVen;

	@Column(name="subtotal_ven", nullable=false, precision=5, scale=2)
	private BigDecimal subtotalVen;

	@Column(name="total_ven", nullable=false, precision=5, scale=2)
	private BigDecimal totalVen;

	//bi-directional many-to-one association to VenDetalleVenta
	@OneToMany(mappedBy="venVenta")
	private List<VenDetalleVenta> venDetalleVentas;

	//bi-directional many-to-one association to EcoCliente
	@ManyToOne
	@JoinColumn(name="id_eco_cliente")
	private EcoCliente ecoCliente;

	//bi-directional many-to-one association to SegUsuario
	@ManyToOne
	@JoinColumn(name="id_seg_usuario")
	private SegUsuario segUsuario;

	public VenVenta() {
	}

	public Integer getIdVenVenta() {
		return this.idVenVenta;
	}

	public void setIdVenVenta(Integer idVenVenta) {
		this.idVenVenta = idVenVenta;
	}

	public Date getFechaVen() {
		return this.fechaVen;
	}

	public void setFechaVen(Date fechaVen) {
		this.fechaVen = fechaVen;
	}

	public BigDecimal getImpuestoVen() {
		return this.impuestoVen;
	}

	public void setImpuestoVen(BigDecimal impuestoVen) {
		this.impuestoVen = impuestoVen;
	}

	public String getIvaVen() {
		return this.ivaVen;
	}

	public void setIvaVen(String ivaVen) {
		this.ivaVen = ivaVen;
	}

	public BigDecimal getSubtotalVen() {
		return this.subtotalVen;
	}

	public void setSubtotalVen(BigDecimal subtotalVen) {
		this.subtotalVen = subtotalVen;
	}

	public BigDecimal getTotalVen() {
		return this.totalVen;
	}

	public void setTotalVen(BigDecimal totalVen) {
		this.totalVen = totalVen;
	}

	public List<VenDetalleVenta> getVenDetalleVentas() {
		return this.venDetalleVentas;
	}

	public void setVenDetalleVentas(List<VenDetalleVenta> venDetalleVentas) {
		this.venDetalleVentas = venDetalleVentas;
	}

	public VenDetalleVenta addVenDetalleVenta(VenDetalleVenta venDetalleVenta) {
		getVenDetalleVentas().add(venDetalleVenta);
		venDetalleVenta.setVenVenta(this);

		return venDetalleVenta;
	}

	public VenDetalleVenta removeVenDetalleVenta(VenDetalleVenta venDetalleVenta) {
		getVenDetalleVentas().remove(venDetalleVenta);
		venDetalleVenta.setVenVenta(null);

		return venDetalleVenta;
	}

	public EcoCliente getEcoCliente() {
		return this.ecoCliente;
	}

	public void setEcoCliente(EcoCliente ecoCliente) {
		this.ecoCliente = ecoCliente;
	}

	public SegUsuario getSegUsuario() {
		return this.segUsuario;
	}

	public void setSegUsuario(SegUsuario segUsuario) {
		this.segUsuario = segUsuario;
	}

}