package panaderiademo.model.core.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;


/**
 * The persistent class for the inv_producto database table.
 * 
 */
@Entity
@Table(name="inv_producto")
@NamedQuery(name="InvProducto.findAll", query="SELECT i FROM InvProducto i")
public class InvProducto implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_inv_producto", unique=true, nullable=false)
	private Integer idInvProducto;

	@Column(name="cantidad_pro", nullable=false, length=1024)
	private String cantidadPro;

	@Column(name="estado_pro", nullable=false)
	private Boolean estadoPro;

	@Column(name="nombre_pro", nullable=false, length=1024)
	private String nombrePro;

	@Column(name="precio_pro", nullable=false, precision=5, scale=2)
	private BigDecimal precioPro;

	@Column(name="url_imagen_pro", length=1024)
	private String urlImagenPro;

	//bi-directional many-to-one association to InvCategoria
	@ManyToOne
	@JoinColumn(name="id_inv_categoria")
	private InvCategoria invCategoria;

	//bi-directional many-to-one association to VenDetalleVenta
	@OneToMany(mappedBy="invProducto")
	private List<VenDetalleVenta> venDetalleVentas;

	public InvProducto() {
	}

	public Integer getIdInvProducto() {
		return this.idInvProducto;
	}

	public void setIdInvProducto(Integer idInvProducto) {
		this.idInvProducto = idInvProducto;
	}

	public String getCantidadPro() {
		return this.cantidadPro;
	}

	public void setCantidadPro(String cantidadPro) {
		this.cantidadPro = cantidadPro;
	}

	public Boolean getEstadoPro() {
		return this.estadoPro;
	}

	public void setEstadoPro(Boolean estadoPro) {
		this.estadoPro = estadoPro;
	}

	public String getNombrePro() {
		return this.nombrePro;
	}

	public void setNombrePro(String nombrePro) {
		this.nombrePro = nombrePro;
	}

	public BigDecimal getPrecioPro() {
		return this.precioPro;
	}

	public void setPrecioPro(BigDecimal precioPro) {
		this.precioPro = precioPro;
	}

	public String getUrlImagenPro() {
		return this.urlImagenPro;
	}

	public void setUrlImagenPro(String urlImagenPro) {
		this.urlImagenPro = urlImagenPro;
	}

	public InvCategoria getInvCategoria() {
		return this.invCategoria;
	}

	public void setInvCategoria(InvCategoria invCategoria) {
		this.invCategoria = invCategoria;
	}

	public List<VenDetalleVenta> getVenDetalleVentas() {
		return this.venDetalleVentas;
	}

	public void setVenDetalleVentas(List<VenDetalleVenta> venDetalleVentas) {
		this.venDetalleVentas = venDetalleVentas;
	}

	public VenDetalleVenta addVenDetalleVenta(VenDetalleVenta venDetalleVenta) {
		getVenDetalleVentas().add(venDetalleVenta);
		venDetalleVenta.setInvProducto(this);

		return venDetalleVenta;
	}

	public VenDetalleVenta removeVenDetalleVenta(VenDetalleVenta venDetalleVenta) {
		getVenDetalleVentas().remove(venDetalleVenta);
		venDetalleVenta.setInvProducto(null);

		return venDetalleVenta;
	}

}