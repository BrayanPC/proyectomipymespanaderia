package panaderiademo.model.produccion.managers;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import panaderiademo.model.core.entities.ProMateriaPrima;
import panaderiademo.model.core.entities.ProMedida;
import panaderiademo.model.core.managers.ManagerDAO;

/**
 * Session Bean implementation class ManagerProduccion
 */
@Stateless
@LocalBean
public class ManagerProduccion {
	@EJB
	private ManagerDAO mDAO;

	/**
	 * Default constructor.
	 */
	public ManagerProduccion() {

	}

	public List<ProMedida> findAllProMedida() {
		List<ProMedida> listaMedidas = mDAO.findAll(ProMedida.class);
		return listaMedidas;
	}

	public List<ProMateriaPrima> findAllProMateriaPrima() {
		List<ProMateriaPrima> listaMateriaPrima = mDAO.findAll(ProMateriaPrima.class, "idProMateriaPrima");
		return listaMateriaPrima;
	}

	public ProMedida findProMedidaById(int idProMedida) throws Exception {
		ProMedida medida = (ProMedida) mDAO.findById(ProMedida.class, idProMedida);
		return medida;
	}

	public void insertarProducto(ProMateriaPrima nuevaMateriaPrima) throws Exception {
//		ProMateriaPrima materiaPrima = (ProMateriaPrima) mDAO.findById(ProMateriaPrima.class,nuevaMateriaPrima.getIdProMateriaPrima());
		mDAO.insertar(nuevaMateriaPrima);
	}
	public void insertarMedida(ProMedida nuevaMedida) throws Exception {
		mDAO.insertar(nuevaMedida);
	}

	public void actualizarProducto(ProMateriaPrima materiaPrimaEdicion) throws Exception {
		ProMateriaPrima materiaPrima = (ProMateriaPrima) mDAO.findById(ProMateriaPrima.class,
				materiaPrimaEdicion.getIdProMateriaPrima());
		materiaPrima.setNombreMat(materiaPrimaEdicion.getNombreMat());
		materiaPrima.setProMedida(materiaPrimaEdicion.getProMedida());
		materiaPrima.setCantidadMat(materiaPrimaEdicion.getCantidadMat());
		mDAO.actualizar(materiaPrima);
	}

	public void eliminarProducto(ProMateriaPrima materiaPrima) throws Exception {
		mDAO.eliminar(ProMateriaPrima.class, materiaPrima.getIdProMateriaPrima());
	}
	
	public void eliminarMedida(ProMedida medida) throws Exception {
		mDAO.eliminar(ProMedida.class, medida.getIdProMedida());
	}

	public void cambiarEstado(ProMateriaPrima materiaPrimaEstado) throws Exception {
		ProMateriaPrima materiaPrima = (ProMateriaPrima) mDAO.findById(ProMateriaPrima.class,materiaPrimaEstado.getIdProMateriaPrima());
		materiaPrima.setEstadoMat(!materiaPrima.getEstadoMat());
		mDAO.actualizar(materiaPrima);
	}

}
