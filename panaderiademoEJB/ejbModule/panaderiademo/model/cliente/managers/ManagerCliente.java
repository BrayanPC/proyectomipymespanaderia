package panaderiademo.model.cliente.managers;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import panaderiademo.model.core.entities.EcoCliente;
import panaderiademo.model.core.entities.SegUsuario;
import panaderiademo.model.seguridades.dtos.LoginDTO;

/**
 * Session Bean implementation class ManagerCliente
 */
@Stateless
@LocalBean
public class ManagerCliente {
	@PersistenceContext
	private EntityManager em;

	/**
	 * Default constructor.
	 */
	public ManagerCliente() {
		// TODO Auto-generated constructor stub
	}

	public List<EcoCliente> findAllClientes() {
		String consulta = "select o from " + EcoCliente.class.getSimpleName() + " o";
		Query q = em.createQuery(consulta);
		return q.getResultList();
	}

	public EcoCliente findClienteById(int idCLi) {
		return em.find(EcoCliente.class, idCLi);
	}

	public void insertarCliente(EcoCliente cliente) throws Exception {
		cliente.setClaveCli("");
		em.persist(cliente);
	}

	public void registrarCliente(EcoCliente cliente) throws Exception {
		String consulta = "select o from " + EcoCliente.class.getSimpleName() + " o WHERE o.cedulaCli = '"
				+ cliente.getCedulaCli() + "'";
		Query q = em.createQuery(consulta);
		EcoCliente ecoCliente = (EcoCliente) q.getSingleResult();
		if (ecoCliente != null && ecoCliente.getClaveCli().length() > 0) {
			throw new Exception("Cliente Ya registrado.");
		} else {
			if (ecoCliente != null) {
				ecoCliente.setNombreCli(cliente.getNombreCli());
				ecoCliente.setApellidoCli(cliente.getApellidoCli());
				ecoCliente.setCorreoCli(cliente.getCorreoCli());
				ecoCliente.setClaveCli(cliente.getClaveCli());
				ecoCliente.setTelefonoCli(cliente.getTelefonoCli());
				em.merge(ecoCliente);
			} else {
				em.persist(cliente);
			}
		}
	}

	public void eliminarCliente(int idCli) {
		EcoCliente cliente = findClienteById(idCli);
		if (cliente != null)
			em.remove(cliente);
	}

	public void actualizarCliente(EcoCliente edicionCliente) throws Exception {
		EcoCliente c = findClienteById(edicionCliente.getIdEcoCliente());
		if (c == null)
			throw new Exception("No existe el cliente con la cedula specificada.");
		c.setCedulaCli(edicionCliente.getCedulaCli());
		c.setNombreCli(edicionCliente.getNombreCli());
		c.setApellidoCli(edicionCliente.getApellidoCli());
		c.setCorreoCli(edicionCliente.getCorreoCli());
		c.setClaveCli(edicionCliente.getCedulaCli());
		c.setTelefonoCli(edicionCliente.getTelefonoCli());
		em.merge(c);
	}

}
