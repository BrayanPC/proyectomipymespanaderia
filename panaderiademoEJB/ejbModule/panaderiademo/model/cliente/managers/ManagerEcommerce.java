package panaderiademo.model.cliente.managers;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import panaderiademo.model.core.entities.EcoCliente;
import panaderiademo.model.core.entities.InvCategoria;
import panaderiademo.model.core.entities.InvProducto;
import panaderiademo.model.core.entities.SegUsuario;
import panaderiademo.model.core.entities.VenDetalleVenta;
import panaderiademo.model.core.entities.VenImpuesto;
import panaderiademo.model.core.entities.VenVenta;
import panaderiademo.model.core.managers.ManagerDAO;
import panaderiademo.model.productos.dtos.ProductoDTO;
import panaderiademo.model.seguridades.dtos.LoginDTO;
import panaderiademo.model.ventas.dtos.DetalleVentaDTO;
import panaderiademo.model.ventas.dtos.VentaDTO;

/**
 * Session Bean implementation class ManagerCliente
 */
@Stateless
@LocalBean
public class ManagerEcommerce {
	@EJB
	private ManagerDAO mDAO;

	/**
	 * Default constructor.
	 */
	public ManagerEcommerce() {
		// TODO Auto-generated constructor stub
	}

	public List<ProductoDTO> findProductosByCatId(int idCat, String nombreP) {
		List<InvProducto> listaProductos = new ArrayList<InvProducto>();
		List<ProductoDTO> listaProductosDTO = new ArrayList<ProductoDTO>();
		String clausula = "";
		if (idCat == 0) {
			if (!nombreP.equals("")) {
				clausula = "nombre_pro LIKE '" + nombreP + "%' or nombre_pro LIKE '%" + nombreP
						+ "%' or nombre_pro LIKE '%" + nombreP + "'";
				listaProductos = mDAO.findWhere(InvProducto.class, clausula, "");
			} else {
				listaProductos = mDAO.findAll(InvProducto.class);
			}
		} else {
			if (!nombreP.equals("")) {
				clausula = "nombre_pro LIKE '" + nombreP + "%' or nombre_pro LIKE '%" + nombreP
						+ "%' or nombre_pro LIKE '%" + nombreP + "' and id_inv_categoria = " + idCat;
				listaProductos = mDAO.findWhere(InvProducto.class, clausula, "");
			} else {
				clausula = "id_inv_categoria = " + idCat;
				listaProductos = mDAO.findWhere(InvProducto.class, clausula, "");
			}
		}

		for (InvProducto pro : listaProductos) {
			ProductoDTO producto = new ProductoDTO();
			producto.setIdPro(pro.getIdInvProducto());
			producto.setNombrePro(pro.getNombrePro());
			producto.setUrlImg(pro.getUrlImagenPro());
			producto.setPrecioPro(pro.getPrecioPro().doubleValue());
			listaProductosDTO.add(producto);
		}
		return listaProductosDTO;
	}

	public VenImpuesto findImpuesto() throws Exception {
		VenImpuesto impuesto = (VenImpuesto) mDAO.findById(VenImpuesto.class, 1);
		return impuesto;
	}

	public List<InvCategoria> findAllCategorias() {
		List<InvCategoria> listaCategorias = new ArrayList<InvCategoria>();
		listaCategorias = mDAO.findAll(InvCategoria.class);
		InvCategoria categoria = new InvCategoria();
		categoria.setIdInvCategoria(0);
		categoria.setNombreCat("Todas");
		categoria.setDescripcionCat("");
		listaCategorias.add(categoria);
		return listaCategorias;
	}

	public InvProducto findProductoById(int idPro) throws Exception {
		InvProducto pro = (InvProducto) mDAO.findById(InvProducto.class, idPro);
		if (pro == null) {
			throw new Exception("Seleccione Un Producto.");
		}
		return pro;
	}

	public VentaDTO calcularTotal(List<DetalleVentaDTO> listado, double valorImp) {
		double subtotal = 0;
		for (DetalleVentaDTO detVen : listado)
			subtotal += detVen.getTotal();
		VentaDTO venta = new VentaDTO();
		venta.setSubtotal(subtotal);
		double impuesto = subtotal * valorImp;
		venta.setImpuesto(impuesto);
		double total = subtotal + impuesto;
		venta.setTotal(total);
		return venta;
	}

	public List<DetalleVentaDTO> eliminarProductoDetalleVenta(List<DetalleVentaDTO> lista, int idDetVen) {
		if (lista == null)
			return null;
		int i = 0;
		for (DetalleVentaDTO detVen : lista) {
			if (detVen.getIdDetVen() == idDetVen) {
				lista.remove(i);
				break;
			}
			i++;
		}
		return lista;
	}
	
	public VentaDTO registrarVenta(VentaDTO ventaDTO, List<DetalleVentaDTO> listaDetalleVenta) throws Exception {
		VenVenta venta = new VenVenta();
		EcoCliente cliente = new EcoCliente();
		if (ventaDTO.getIdCLi() != 0) {
			cliente = (EcoCliente) mDAO.findById(EcoCliente.class, ventaDTO.getIdCLi());
			venta.setEcoCliente(cliente);
		}
		SegUsuario usuario = (SegUsuario) mDAO.findById(SegUsuario.class, ventaDTO.getIdUsu());
		venta.setSegUsuario(usuario);
		venta.setFechaVen(new Date());
		venta.setIvaVen(ventaDTO.getIva());
		venta.setSubtotalVen(new BigDecimal(ventaDTO.getSubtotal()));
		venta.setImpuestoVen(new BigDecimal(ventaDTO.getImpuesto()));
		venta.setTotalVen(new BigDecimal(ventaDTO.getTotal()));
		mDAO.insertar(venta);
		ventaDTO.setIdVen(venta.getIdVenVenta());
		for (DetalleVentaDTO detVen : listaDetalleVenta) {
			VenDetalleVenta detalleVenta = new VenDetalleVenta();
			detalleVenta.setVenVenta(venta);
			InvProducto producto = (InvProducto) mDAO.findById(InvProducto.class, detVen.getIdPro());
			detalleVenta.setInvProducto(producto);
			detalleVenta.setCantidadVen(detVen.getCantidad());
			detalleVenta.setPrecioVen(new BigDecimal(detVen.getPrecio()));
			detalleVenta.setTotalVen(new BigDecimal(detVen.getTotal()));
			mDAO.insertar(detalleVenta);
		}

		return ventaDTO;
	}

}
